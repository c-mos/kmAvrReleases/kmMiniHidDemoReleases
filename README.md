# kmMiniHidDemoReleases

This repository contains binary releases for an AVR project [kmMiniHidDemo](https://gitlab.com/c-mos/avr/kmMiniHidDemou). Below are the available releases along with their descriptions:

## Releases

### Release v1.0

- **File:** [kmMiniHidDemo_v1.0.0.hex](https://gitlab.com/c-mos/kmAvrReleases/kmMiniHidDemoReleases/-/raw/main/kmMiniHidDemo_v1.0.0.hex?ref_type=heads)
- **Description:** Initial release kmMiniHidDemo project.

## Instructions

To use the binary releases:

1. Download the appropriate `.hex` file for your AVR device from the links above.
2. Flash the downloaded `.hex` file onto your AVR device using your preferred programming tool or software.

## Author and License
Author: Krzysztof Moskwa
License: GNU General Public License (GPL) version 3.0 or later. See LICENSE file for details.
